const express = require('express');
const router = express.Router();
const client = require('./db.js');

router.get('/long/:longitude/lat/:latitude', function (req, res) {

    findPolygon(req.params.longitude, req.params.latitude).then(function (results) {
        res.send(results);
    }).catch(function(err) {
        res.status(500).send(err.toString());
    });
});

// Set the database and collection to use
const setCollection = async () => {
    const db = await client.getDB().db('betterwmu');
    return await db.collection('wmu');
};

// Query the database for any polygons the long, lat value intersects with
const findPolygon = async function (long, lat) {

    try {

        await validateLatitudeAndLongitude(long,lat);

        const collection = await setCollection();

        let col = await collection.find({
            "features.geometry": {
                "$geoIntersects": {
                    "$geometry": {
                        "type": "Point",
                        "coordinates": [parseFloat(long), parseFloat(lat)]
                    }
                }
            }
        });
        
        return col.toArray();
    }
    catch (err) {
        throw err;
    }
};

// Validates the longitude and latitude parameters passed in
const validateLatitudeAndLongitude = async function(long, lat) {

    if(long == null) {
        throw new Error('Missing longitude parameter');
    }
    else if (lat == null) {
        throw new Error('Missing latitude parameter');
    }
    else if (long < -180 || long > 180) {
        throw new Error('Longitude value should be between -180 and 180 degress');
    }
    else if (lat < -90 || lat > 90) {
        throw new Error('Latitude value should be between -90 and 90 degress');
    }
};

// Exports
module.exports = router;
module.exports.setCollection = setCollection;
module.exports.findPolygon = findPolygon;
module.exports.validateLatitudeAndLongitude = validateLatitudeAndLongitude;
