const mongoClient = require('mongodb').MongoClient;

// Database properties
const url = 'mongodb://mongodb:27017';

let _db = null;

async function connect() {
    _db = await mongoClient.connect(url);
    return _db;
}

function getDB() {
    return _db;
}

module.exports.connect = connect;
module.exports.getDB = getDB;