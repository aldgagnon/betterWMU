const chai = require('chai');
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;
const index = require('../routes/index.js');

describe('findPolygon', function () {
    it('should reject if findPolygon fails when querying with an invalid collection object', async () => {
        index.setCollection = function() {
            return Promise.reject()
        };

        return expect(index.findPolygon(null,null)).to.be.rejected;
    });

});

describe('validateLatitudeAndLongitude', function () {

    it('should reject if given a null longitude', async () => {
        return expect(index.validateLatitudeAndLongitude(null,5)).to.be.rejected;
    });

    it('should reject if given a null latitude', async () => {
        return expect(index.validateLatitudeAndLongitude(5,null)).to.be.rejected;
    });

    it('should reject if given a longitude greater then 180', async () => {
        return expect(index.validateLatitudeAndLongitude(181,0)).to.be.rejected;
    });

    it('should reject if given a longitude less then -180', async () => {
        return expect(index.validateLatitudeAndLongitude(-181,0)).to.be.rejected;
    });

    it('should reject if given a latitude greater then 90', async () => {
        return expect(index.validateLatitudeAndLongitude(0,91)).to.be.rejected;
    });

    it('should reject if given a latitude less then -90', async () => {
        return expect(index.validateLatitudeAndLongitude(0,-91)).to.be.rejected;
    });

    it('should resolve if given a longitude and latitude within acceptable ranges', async () => {
        return expect(index.validateLatitudeAndLongitude(5,5)).to.be.fulfilled;
    });

    it('should resolve if given a longitude and latitude of (0,0)', async () => {
        return expect(index.validateLatitudeAndLongitude(0,0)).to.be.fulfilled;
    });

});
