# betterWMU

This microservice allows for coordinate-based search of Ontario Wildlife Management Units (WMU).

## Installing

1. Clone this repository 
   - `$ git clone https://gitlab.com/aldgagnon/betterWMU.git`
   
2. Start the Docker container
   - `$ docker-compose up --build -d`