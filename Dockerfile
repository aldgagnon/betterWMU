FROM node:alpine

run apk update
run apk add curl

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY src/package.json /usr/src/app
RUN npm install

# Copy node application source
COPY src /usr/src/app

EXPOSE 3000

# Default to a login shell
CMD ["npm", "start"]
